// console.log("Hello World");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
		
	function printWelcomeMessage(){
		let name = prompt("Enter your name: ");
		let age = prompt("Enter your age: ");
		let location = prompt("Enter your location: ")
		console.log("Hello, " + name);
		console.log("You are " + age + " years old.");
		console.log("You live in " + location);
	}	
		printWelcomeMessage();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

		function printFavBands(){
			let firstBand = "The Beatles";
			let secondBand = "Metallica";
			let thirdBand = "The Eagles";
			let fourthBand = "L'Arc~en~Ciel";
			let fifthBand = "Eraserheads";
			console.log("1. " + firstBand);
			console.log("2. " + secondBand);
			console.log("3. " + thirdBand);
			console.log("4. " + fourthBand);
			console.log("5. " + fifthBand);

		}

		printFavBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

		function printFavMovies(){
			let firstMovie = "The Godfather";
			let secondMovie = "The Godfather, Part II";
			let thirdMovie = "Shawshank Redemption";
			let fourthMovie = "To Kill A Mockingbird";
			let fifthMovie = "Psycho";
			let movie1Rating = "97%";
			let movie2Rating = "96%";
			let movie3Rating = "91%";
			let movie4Rating = "93%";
			let movie5Rating = "96%";
			console.log("1. " + firstMovie);
			console.log("Rotten Tomatoes Rating: " + movie1Rating);
			console.log("2. " + secondMovie);
			console.log("Rotten Tomatoes Rating: " + movie2Rating);
			console.log("3. " + thirdMovie);
			console.log("Rotten Tomatoes Rating: " + movie3Rating);
			console.log("4. " + fourthMovie);
			console.log("Rotten Tomatoes Rating: " + movie4Rating);
			console.log("5. " + fifthMovie);
			console.log("Rotten Tomatoes Rating: " + movie5Rating);

		}

		printFavMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
	
};

	

	printFriends();

